import java.util.Scanner;

public class number2 {
    public static void power(double number, int x) {

        double counter = number;

        for (int i = 1; i < x; i++) {
            counter *= number;
        }

        System.out.println(counter);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double number = scanner.nextDouble();
        int x = scanner.nextInt();

        power(number, x);
    }



}
